
Changlog:

August 2008
-----------
 * Change case of package 'User interface' (aaron).

January 2008
------------
 * added optional support for jq module (aaron).
 * added theme_magnifier (aaron).
 * added support for id attributes (aaron).
 * allow image src for magnified image (aaron).
