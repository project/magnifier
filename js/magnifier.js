/*
  jQuery image magnification plugin - ported from dojox.image.Magnifier class
  from the Dojo Toolkit, released dual AFL/BSD license, free for use.
  @author: Peter Higgins - dante@dojotoolkit.org

  @param: scale - scaling factor from small image to viewport
  @param: glassSize - w/h of the overlay

  @example $("img.mag").magnify({ scale:3 });
  @example $('img.zooooooom").magnify({ scale:10, glassSize:250 });
  @returns: jQuery

 */

if (!jQuery.fn.offset) {

// The Offset Method
// Originally By Brandon Aaron, part of the Dimension Plugin
// http://jquery.com/plugins/project/dimensions
jQuery.fn.offset = function() {
  var left = 0, top = 0, elem = this[0], results;

  if ( elem ) with ( jQuery.browser ) {
    var absolute     = jQuery.css(elem, "position") == "absolute",
        parent       = elem.parentNode,
        offsetParent = elem.offsetParent,
        doc          = elem.ownerDocument,
        safari2      = safari && parseInt(version) < 522;

    // Use getBoundingClientRect if available
    if ( elem.getBoundingClientRect ) {
      box = elem.getBoundingClientRect();

      // Add the document scroll offsets
      add(
        box.left + Math.max(doc.documentElement.scrollLeft, doc.body.scrollLeft),
        box.top  + Math.max(doc.documentElement.scrollTop,  doc.body.scrollTop)
      );

      // IE adds the HTML element's border, by default it is medium which is 2px
      // IE 6 and IE 7 quirks mode the border width is overwritable by the following css html { border: 0; }
      // IE 7 standards mode, the border is always 2px
      if ( msie ) {
        var border = jQuery("html").css("borderWidth");
        border = (border == "medium" || jQuery.boxModel && parseInt(version) >= 7) && 2 || border;
        add( -border, -border );
      }

    // Otherwise loop through the offsetParents and parentNodes
    } else {

      // Initial element offsets
      add( elem.offsetLeft, elem.offsetTop );

      // Get parent offsets
      while ( offsetParent ) {
        // Add offsetParent offsets
        add( offsetParent.offsetLeft, offsetParent.offsetTop );

        // Mozilla and Safari > 2 does not include the border on offset parents
        // However Mozilla adds the border for table cells
        if ( mozilla && /^t[d|h]$/i.test(parent.tagName) || !safari2 )
          border( offsetParent );

        // Safari <= 2 doubles body offsets with an absolutely positioned element or parent
        if ( safari2 && !absolute && jQuery.css(offsetParent, "position") == "absolute" )
          absolute = true;

        // Get next offsetParent
        offsetParent = offsetParent.offsetParent;
      }

      // Get parent scroll offsets
      while ( parent.tagName && !/^body|html$/i.test(parent.tagName) ) {
        // Work around opera inline/table scrollLeft/Top bug
        if ( !/^inline|table-row.*$/i.test(jQuery.css(parent, "display")) )
          // Subtract parent scroll offsets
          add( -parent.scrollLeft, -parent.scrollTop );

        // Mozilla does not add the border for a parent that has overflow != visible
        if ( mozilla && jQuery.css(parent, "overflow") != "visible" )
          border( parent );

        // Get next parent
        parent = parent.parentNode;
      }

      // Safari doubles body offsets with an absolutely positioned element or parent
      if ( safari2 && absolute )
        add( -doc.body.offsetLeft, -doc.body.offsetTop );
    }

    // Return an object with top and left properties
    results = { top: top, left: left };
  }

  return results;

  function border(elem) {
    add( jQuery.css(elem, "borderLeftWidth"), jQuery.css(elem, "borderTopWidth") );
  }

  function add(l, t) {
    left += parseInt(l) || 0;
    top += parseInt(t) || 0;
  }
};

}

jQuery.fn.magnify = function(args){
    return this.each(function(){

    // setup args
    if(!args.glassSize){ args.glassSize = 125; }
    if(!args.scale){ args.scale = 4; }
    if(!args.image){ args.image = this.src; }

    // get our size
    var w = $(this).width();
    var h = $(this).height();

    // size the image
    var _zoomSize = {
      w: w * args.scale,
      h: h * args.scale
    };

    // FIXME: how to scope in jQuery?
    var m = {
      // properties:
      srcNode: null,
      overlay: null,
      img: null,
      size: _zoomSize,

      _show:function(e){
        // show the overlay
        m.overlay.css({ visibility:"visible", display:"block" });
        m._position(e);
      },
      _hide:function(e){
        // hide the overlay
        m.overlay.css({ visibility:"hidden", display:"none" });
      },

      _position:function(e){
        // position the overlay under the mouse
        var l = Math.floor(e.pageX - (w/2));
        var t = Math.floor(e.pageY - (h/2));

        $(m.overlay).css({
          top: t+"px",
          left: l+"px"
        });

        // and position the image inside the overlay relatively
        var _off = $(m.srcNode).offset({ scroll:true });
        var xOff = (e.pageX - _off.left) / w;
        var yOff = (e.pageY - _off.top) / h;
        var x = (m.size.w * xOff * -1)+(w * xOff);
        var y = (m.size.h * yOff * -1)+(h * yOff);
        $(m.img).css({
          top: y+"px",
          left: x+"px"
        });

      }
    };

    // scoped! ;)
    m.srcNode = this;
    $(this).mouseover(m._show);

    // mmm a template system would be easy to do
    // add a node to the body and size and position it
    m.overlay = $("<div></div>")
      .appendTo("body")
      .addClass("glassNode")
      .mousemove(m._position)
      .mouseout(m._hide)
      .css({
        overflow:"hidden",
        width:args.glassSize+"px",
        height:args.glassSize+"px"
      });

    // add the scaled image to the floating container, and set it's size.
    m.img = $("<img src='"+args.image+"' class='glassInner' />")
      .appendTo(m.overlay)
      .css({
        position:"relative",
        top:0, left:0,
        width: _zoomSize.w + "px",
        height: _zoomSize.h + "px"
      });
    }); // jQuery
};
